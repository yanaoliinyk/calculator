package calculator;

import service.ExpressionService;

public class Calculator {

    public int calculate(String enteredLine){
        ExpressionService expressionService = new ExpressionService();
        Expression expression = expressionService.createExpression(enteredLine);
        return expressionService.executeOperations(expression);
    }

}
