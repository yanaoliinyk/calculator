package calculator;

import calculator.operation.Operation;

public class Expression {
    private int[] operands;
    private Operation[] operations;

    public Expression(int[] operands, Operation[] operations) {
        this.operands = operands;
        this.operations = operations;
    }

    public void setOperands(int[] operands) {
        this.operands = operands;
    }

    public void setOperations(Operation[] operations) {
        this.operations = operations;
    }

    public int[] getOperands() {
        return operands;
    }

    public Operation[] getOperations() {
        return operations;
    }
}
