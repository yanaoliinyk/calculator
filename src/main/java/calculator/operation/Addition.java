package calculator.operation;

public class Addition extends Operation {

    public Addition() {
        setPriority(OperationPriority.SECOND);
    }

    @Override
    public int perform(int firstOperand, int secondOperand) {
        return firstOperand + secondOperand;
    }
}
