package calculator.operation;

public class Division extends Operation {

    public Division() {
        setPriority(OperationPriority.FIRST);
    }

    @Override
    public int perform(int firstOperand, int secondOperand) {
        return firstOperand / secondOperand;
    }
}
