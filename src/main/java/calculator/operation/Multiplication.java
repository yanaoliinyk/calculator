package calculator.operation;

public class Multiplication extends Operation {

    public Multiplication() {
        setPriority(OperationPriority.FIRST);
    }

    @Override
    public int perform(int firstOperand, int secondOperand) {
        return firstOperand * secondOperand;
    }
}
