package calculator.operation;

public abstract class Operation {
    private OperationPriority priority;

    public OperationPriority getPriority() {
        return priority;
    }

    protected void setPriority(OperationPriority priority){
        this.priority = priority;
    }

    public abstract int perform(int firstOperand, int secondOperand);
}
