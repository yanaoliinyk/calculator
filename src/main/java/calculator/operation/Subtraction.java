package calculator.operation;

public class Subtraction extends Operation {

    public Subtraction() {
        setPriority(OperationPriority.SECOND);
    }

    @Override
    public int perform(int firstOperand, int secondOperand) {
        return firstOperand - secondOperand;
    }
}
