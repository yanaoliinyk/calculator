package exceptions;

class CalculatorException extends RuntimeException {
    CalculatorException(String message) {
        super(message);
    }
}
