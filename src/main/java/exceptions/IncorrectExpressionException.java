package exceptions;

public class IncorrectExpressionException extends CalculatorException{
    public IncorrectExpressionException(String message) {
        super(message);
    }
}
