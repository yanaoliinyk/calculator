package service;

import calculator.Expression;
import calculator.operation.Operation;
import calculator.operation.OperationPriority;
import org.apache.commons.lang3.ArrayUtils;
import utils.ConvertationUtil;
import utils.StringUtil;

import java.util.Arrays;

public class ExpressionService {

    public Expression createExpression(String expressionLine) {
        expressionLine = StringUtil.getValidExpression(expressionLine);
        Operation[] operations = Arrays.stream(StringUtil.getOperations(expressionLine))
                .map(OperationService.operations::get).toArray(Operation[]::new);
        int[] operands = ConvertationUtil.convertStringArrayToIntArray(StringUtil.getNumbers(expressionLine));
        return new Expression(operands, operations);
    }

    public int executeOperations(Expression expression) {
        return executePriorityOperations(executePriorityOperations
                (expression, OperationPriority.FIRST), OperationPriority.SECOND).getOperands()[0];
    }

    private Expression executePriorityOperations(Expression expression, OperationPriority operationPriority) {
        Operation[] operations = expression.getOperations();
        int[] operands = expression.getOperands();
        for (int i = 0; i < operations.length; i++) {
            if (operations[i].getPriority().equals(operationPriority)) {
                int result = operations[i].perform(operands[i], operands[i + 1]);
                operations = ArrayUtils.remove(operations, i);
                operands = ArrayUtils.remove(operands, i);
                operands[i] = result;
                i--;
            }
        }
        expression.setOperations(operations);
        expression.setOperands(operands);
        return expression;
    }

}
