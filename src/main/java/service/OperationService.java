package service;

import calculator.operation.*;

import java.util.HashMap;
import java.util.Map;

public class OperationService {
    public static final Map<String, Operation> operations = createOperations();

    private static Map<String, Operation> createOperations(){
        Map<String, Operation> operations = new HashMap<>();
        operations.put("+", new Addition());
        operations.put("-", new Subtraction());
        operations.put("*", new Multiplication());
        operations.put("/", new Division());
        return operations;
    }
}
