package utils;

public class ConvertationUtil {

    public static int[] convertStringArrayToIntArray(String[] numbers) {
        int[] numbersConverted = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            numbersConverted[i] = Integer.parseInt(numbers[i]);
        }
        return numbersConverted;
    }
}
