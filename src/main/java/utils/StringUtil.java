package utils;

import exceptions.IncorrectExpressionException;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;

public class StringUtil {
    private static final String NUMBERS_REGEX = "[\\d]+";
    private static final String OPERATIONS_REGEX = "[-+/*]";
    private static final String INCORRECT_SYMBOLS_REGEX = ".*[^(\\-+/*\\d)].*";

    public static String[] getOperations(String line) {
        return ArrayUtils.removeElement(line.split(NUMBERS_REGEX), "");
    }

    public static String[] getNumbers(String line) {
        return line.split(OPERATIONS_REGEX);
    }

    public static String getValidExpression(String expression) {
        if (expression.matches(INCORRECT_SYMBOLS_REGEX)) {
            throw new IncorrectExpressionException("Expression had wrong symbols");
        }
        if (expression.matches(".*[^\\d]$")) {
            throw new IncorrectExpressionException("Expression ended with non digit symbol");
        }
        if (expression.matches("^[^(\\d-)].*")) {
            throw new IncorrectExpressionException("Expression started with non digit or minus symbol");
        }
        if(expression.matches(".*([-+/*]){2,}.*")){
            throw new IncorrectExpressionException("Expression contained 2 operations one by one");
        }
        if(expression.matches("^[-+].*")){
            return "0".concat(expression);
        }
        return expression;
    }
}
