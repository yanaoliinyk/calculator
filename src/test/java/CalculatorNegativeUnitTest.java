import calculator.Calculator;
import exceptions.IncorrectExpressionException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculatorNegativeUnitTest {
    private String expression;

    private Calculator calculator = new Calculator();

    public CalculatorNegativeUnitTest(String expression) {
        this.expression = expression;
    }

    @Parameters
    public static Collection data() {
        return Arrays.asList("lknl", "45+-114", "*4*2-3/1", "-9+8+77/7-7/9*2-");
    }

    @Test(expected = IncorrectExpressionException.class)
    public void shouldThrowException() {
        calculator.calculate(expression);
    }
}
