import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import calculator.Calculator;

@RunWith(Parameterized.class)
public class CalculatorPositiveUnitTest {
    private String expression;
    private int expectedResult;

    private Calculator calculator = new Calculator();

    public CalculatorPositiveUnitTest(String expression, int expectedResult) {
        this.expression = expression;
        this.expectedResult = expectedResult;
    }

    @Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][]{
                {"4-8/4", 2},
                {"45+114", 159},
                {"4*2-3/1", 5},
                {"-9+8+4/3", 0}});
    }

    @Test
    public void shouldReturnResult() {
        int actual = calculator.calculate(expression);
        Assert.assertEquals(expectedResult, actual);
    }
}
